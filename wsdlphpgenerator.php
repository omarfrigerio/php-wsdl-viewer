<?php
    $wsdl = new SoapClient("WSDL_URL");
    $functions = $wsdl->__getFunctions();
    $types = $wsdl->__getTypes();

    $html = "";

    $html .= "<h1>Methods</h1>";
    $html .= "<table>";
    foreach($functions as $function){
        $html .= "<tr style='vertical-align:top'><td>".$function."</td></tr>";
    }
    $html .= "</table>";

    $html .= "<h1>Definitions</h1>";
    $html .= "<table>";
    foreach($types as $type){

        preg_match("/(\{)(.*)(\})/s", $type, $matches);
        $functionArgs = str_replace(";", "<br>", $matches[2]);
        $functionName = str_replace($matches[0], "", $type);
        $html .= "<tr style='vertical-align:top'><td>".$functionName."</td><td>".$functionArgs."</td></tr>";
    }
    $html .= "</table>";

    echo $html;
?>
